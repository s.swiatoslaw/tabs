let tabs = document.querySelectorAll('.tabs-title');
// let labsActive = document.querySelector('.tabs-title.active');
// let contentActive = document.querySelector('.content.is-active');

tabs.forEach(function(element) {
    element.addEventListener('click', function() {
        let tabsClick = document.querySelector(`.content[data-content="${element.dataset.tabs}"]`);
        document.querySelector('.content.is-active').classList.remove('is-active');
        document.querySelector('.tabs-title.active').classList.remove('active');
        tabsClick.classList.add('is-active');
        element.classList.add('active');
        // contentActive.classList.remove('active');
        // labsActive.classList.remove('is-active');
    });
});
